import Vue from 'vue'
import Router from 'vue-router'

import {
    HomePage,
    GamePage,
} from '@/components/pages'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HomePage',
      component: HomePage,
    },
    {
      path: '/game',
      name: 'GamePage',
      component: GamePage,
    },
  ],
})


export default router
