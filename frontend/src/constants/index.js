
import { colors, player_color_list } from '@/constants/colors'
import { game_cell_type } from '@/constants/game_cell'

export {
    colors,
    player_color_list,
    game_cell_type,
}
