'use strict'
    
const game_cell_type = {
    TYPE_ITEM: 1,
    TYPE_PLAYER_BODY: 2,
    TYPE_PLAYER_HEAD: 3,
}

export {
    game_cell_type,
}
