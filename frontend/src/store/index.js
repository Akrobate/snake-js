import Vue from 'vue'
import Vuex from 'vuex'
import GameStore from './modules/GameStore'
import ColorStore from './modules/ColorStore'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    game_store: GameStore,
    color_store: ColorStore,
  },
})