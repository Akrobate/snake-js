'use strict'

const state = () => ({
  screen_grid: [],
  grid_width: null,
  grid_height: null,
  user_list: [],
  dead_user_list: [],
  websocket_connection: null,
  score: 0,
})

const getters = {
  screenGrid: (_state) => _state.screen_grid,
  websocketConnection: (_state) => _state.websocket_connection,
  getUserList: (_state) => _state.user_list,
  getDeadUserList: (_state) => _state.dead_user_list,
  gridWidth: (_state) => _state.grid_width,
  gridHeight: (_state) => _state.grid_height,
  getScore: (_state) => _state.score,
}

const actions = {
  createWebSocketConnectionIfNotExists({ dispatch, state: _state }, configuration) {
    if (_state.websocket_connection === null) {
      dispatch('createWebSocketConnection', configuration)
    }
    return _state.websocket_connection
  },
  createWebSocketConnection({ commit }, configuration) {
    let connection = null
    try {
      connection = new WebSocket(configuration.websocket_server_url)
      if (connection.readyState === WebSocket.CONNECTING) {
        console.log('Waiting for connection to be etablished')
      } else {
        console.log('createWebSocketConnection connected')
      }
    } catch (error) {
      console.log('StorecreateWebSocketConnection', error)
    }
    commit('set_websocket_connection', connection)
    return state.websocket_connection
  },
  openWsConnectionListner({ state: _state }) {
    _state.websocket_connection.onopen = (event) => {
      console.log(event)
      console.log('Successfully connected to the echo websocket server')
    }
  },
  websocketMessageListner({ state: _state,  dispatch }) {
    _state.websocket_connection.onmessage = (event) => {
      dispatch('processWebsocketMessage', event.data)
    }
  },
  processWebsocketMessage({ commit }, data) {

    const socket_message_data = JSON.parse(data)
    switch (socket_message_data.action) {
      case 'update_grid':
        commit('set_screen_grid', socket_message_data.payload.grid_data)
        commit('set_user_list', socket_message_data.payload.user_list)
        commit('set_dead_user_list', socket_message_data.payload.dead_user_list)
        commit('set_score', socket_message_data.payload.player.score)
        break;
    
      case 'game_params':
        commit('set_grid_width', socket_message_data.payload.grid_width)
        commit('set_grid_height', socket_message_data.payload.grid_height)
        break;
    }

  },
  sendWebsocketMessage({ state: _state }, { action, payload } ) {
    console.log(action, payload)
    return _state
      .websocket_connection
      .send(
        JSON.stringify(
          {
            action,
            payload,
          }
        )
      )
  },
}

const mutations = {
  set_screen_grid(_state, screen_grid) {
    _state.screen_grid = screen_grid
  },
  set_user_list(_state, user_list) {
    _state.user_list = user_list
  },
  set_dead_user_list(_state, dead_user_list) {
    _state.dead_user_list = dead_user_list
  },
  set_grid_width(_state, grid_width) {
    _state.grid_width = grid_width
  },
  set_grid_height(_state, grid_height) {
    _state.grid_height = grid_height
  },
  set_websocket_connection(_state, connection) {
    _state.websocket_connection = connection
  },
  set_score(_state, score) {
    _state.score = score
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
