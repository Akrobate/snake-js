'use strict'

import { player_color_list, colors } from '@/constants'

const state = () => ({
    player_color_list,
    colors,
})

const getters = {
    getColor: (_state) => (color_name) => {
        const found_color = _state.player_color_list.find((color) => color.name === color_name)
        return found_color ? found_color : _state.colors.find((color) => color.name === 'Black')
    },
    getColorHex: (_, getters) => (color_name) => {
        return getters.getColor(color_name).hex
    },
}

export default {
  namespaced: true,
  state,
  getters,
}
