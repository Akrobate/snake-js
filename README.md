# SnakeJS

Socket oriented snake JS game.

- Front end coded with VueJS
- Backend coded with NodeJS 12.10

## Installation

```bash
# Installing backend 
cd backend/
npm install

# Installing frontend
cd frontend/
npm install
```

## Launch the game

```bash
# Strating backend server
cd backend/
npm start

# Strating frontend (in dev mode)
cd frontend/
npm run serve
```

## Todo list

* *04/11/2022* Increase number of items to be relative to number of players
