'use strict';

/**
 * GameCell is the item from the grid
 * @class GameCell
 */
class GameCell {

    // eslint-disable-next-line require-jsdoc
    static build(x, y, type) {
        return new GameCell(x, y, type);
    }

    // eslint-disable-next-line require-jsdoc
    static get TYPE_ITEM() {
        return 1;
    }

    // eslint-disable-next-line require-jsdoc
    static get TYPE_PLAYER_BODY() {
        return 2;
    }

    // eslint-disable-next-line require-jsdoc
    static get TYPE_PLAYER_HEAD() {
        return 3;
    }

    /**
     * @param {Number} x
     * @param {Number} y
     * @param {Number} type
     */
    constructor(x, y, type) {
        this.x = x;
        this.y = y;
        this.type = type;
        this.owner = null;
    }

    // eslint-disable-next-line require-jsdoc
    setOwner(owner) {
        this.owner = owner;
        return this;
    }

    // eslint-disable-next-line require-jsdoc
    setType(type) {
        this.type = type;
        return this;
    }

    // eslint-disable-next-line require-jsdoc
    setX(x) {
        this.x = x;
        return this;
    }

    // eslint-disable-next-line require-jsdoc
    setY(y) {
        this.y = y;
        return this;
    }

    // eslint-disable-next-line require-jsdoc
    getX() {
        return this.x;
    }

    // eslint-disable-next-line require-jsdoc
    getY() {
        return this.y;
    }

    // eslint-disable-next-line require-jsdoc
    getOwner() {
        return this.owner;
    }

    // eslint-disable-next-line require-jsdoc
    getType() {
        return this.type;
    }

    // eslint-disable-next-line require-jsdoc
    isTypePlayerBody() {
        return this.type === GameCell.TYPE_PLAYER_BODY;
    }

}

module.exports = {
    GameCell,
};
