'use strict';

const lodash = require('lodash');

const {
    v4,
} = require('uuid');

const {
    GameCell,
} = require('./GameCell');
class GameSession {

    /**
     * @static
     */
    static get STATUS() {
        return {
            IDLE: 0,
            PAUSED: 2,
            RUNNING: 1,
        };
    }


    /**
     * @static
     */
    static get NUMBER_USER_SECTIONS_ON_START() {
        return 3;
    }


    /**
     * @param {Object} game_params
     * @param {Number} game_params.grid_width
     * @param {Number} game_params.grid_height
     * @param {Number} game_params.main_loop_duration
     * @param {Number} game_params.number_of_items
     * @param {Number} game_params.number_of_items_per_new_user
     * @return {void}
     */
    initGameSession(
        game_params = {}
    ) {

        const default_params = {
            grid_width: 200,
            grid_height: 100,
            main_loop_duration: 100,
            number_of_items: 3,
            number_of_items_per_new_user: 3,
        };

        const {
            grid_width,
            grid_height,
            main_loop_duration,
            number_of_items,
            number_of_items_per_new_user,
        } = {
            ...default_params,
            ...game_params,
        };

        this.user_list = [];
        this.dead_user_list = [];
        this.grid = null;

        this.setGridWidth(grid_width);
        this.setGridHeight(grid_height);

        this.initEmptyGrid();
        this.game_status = GameSession.STATUS.IDLE;
        this.main_loop_duration = main_loop_duration;
        this.interval_handler = null;

        this.items_list = [];
        this.number_of_items = number_of_items;
        this.number_of_items_per_new_user = number_of_items_per_new_user;

        this.game_id = this.generateNewGameId();
    }


    /**
     * @returns {void}
     */
    start() {
        this.game_status = GameSession.STATUS.RUNNING;
        this.interval_handler = setInterval(
            () => {
                this.loop();
            },
            this.main_loop_duration
        );
    }

    /**
     * @returns {void}
     */
    stop() {
        this.game_status = GameSession.STATUS.IDLE;
        clearInterval(this.interval_handler);
        this.interval_handler = null;
    }

    /**
     * @returns {void}
     */
    pause() {
        this.game_status = GameSession.STATUS.PAUSE;
        clearInterval(this.interval_handler);
    }


    /**
     * @returns {void}
     */
    loop() {
        this.processUsersMove();
        this.spawnItemsIfNeeded();
        this.processUsersDeads();

        // Redrawing new grid
        this.initEmptyGrid();
        this.prepareItemsInGrid();
        this.prepareUsersSnakesInGrid();

        this.broadCastGridToAllUsers();
    }


    /**
     * @returns {void}
     */
    initEmptyGrid() {
        const cols = [];
        for (let i = 0; i < this.grid_width; i++) {
            const rows = [];
            for (let j = 0; j < this.grid_height; j++) {
                rows.push(null);
            }
            cols.push(rows);
        }
        this.grid = cols;
    }

    /**
     * @returns {Array<Array>}
     */
    getGrid() {
        return this.grid;
    }

    /**
     * @param {UserGameSession} user
     * @returns {void}
     */
    addUser(user) {
        this.number_of_items += this.number_of_items_per_new_user;
        this.user_list.push(user);
        const spawn_position = this.findZoneToSpawnUser();
        user.initSnakeSectionsList(
            GameSession.NUMBER_USER_SECTIONS_ON_START,
            spawn_position
        );
    }

    /**
     * @param {String} id
     * @returns {Number}
     */
    findUserIndexById(id) {
        return this.user_list.findIndex((user) => user.id === id);
    }

    /**
     * @param {String} id
     * @returns {Object}
     */
    removeUserById(id) {
        this.number_of_items -= this.number_of_items_per_new_user;
        const user_index = this.findUserIndexById(id);
        if (user_index > -1) {
            this.user_list.splice(user_index, 1);
        }
    }

    /**
     * @returns {Number}
     */
    countUsers() {
        return this.user_list.length;
    }

    /**
     * @returns {void}
     */
    prepareUsersSnakesInGrid() {
        this.user_list.forEach((user) => {
            user.getSnakeSectionsList().forEach((section) => {
                this.grid[section.x][section.y] = lodash.cloneDeep(section);
            });
        });
    }

    /**
     * @returns {void}
     */
    prepareItemsInGrid() {
        this.items_list.forEach((item) => {
            this.grid[item.getX()][item.getY()] = item;
        });
    }

    /**
     * @returns {void}
     */
    spawnItemsIfNeeded() {
        while (this.items_list.length < this.number_of_items) {
            this.spawnOneItem();
        }
    }

    /**
     * @returns {void}
     */
    spawnOneItem() {
        let searching_coordinates_for_spawn_item = true;
        let possible_x = null;
        let possible_y = null;
        while (searching_coordinates_for_spawn_item) {
            possible_x = this.randomInteger(this.grid_width);
            possible_y = this.randomInteger(this.grid_height);

            if (this.grid[possible_x][possible_y] === null) {
                searching_coordinates_for_spawn_item = false;
            }
        }

        this.items_list.push(GameCell.build(possible_x, possible_y, GameCell.TYPE_ITEM));
    }

    /**
     * @param {Number} max
     * @returns {Number}
     */
    randomInteger(max) {
        return Math.ceil(Math.random() * max);
    }


    /**
     * @param {*} user
     * @returns {Object}
     */
    findZoneToSpawnUser() {
        const grid_offset_size = 3;
        const space_around_user_hear = 3;

        let searching_zone = true;
        let possible_x = null;
        let possible_y = null;
        while (searching_zone) {
            possible_x = grid_offset_size + this.randomInteger(
                this.grid_width - (grid_offset_size * 2) - 1
            );
            possible_y = grid_offset_size + this.randomInteger(
                this.grid_height - (grid_offset_size * 2) - 1
            );
            if (this.isEmptyZone({
                x: possible_x,
                y: possible_y,
            }, space_around_user_hear)) {
                searching_zone = false;
            }
        }

        return {
            x: possible_x,
            y: possible_y,
        };

    }

    /**
     * @param {Object} coords
     * @param {Number} space_around
     * @return {Boolean}
     */
    isEmptyZone(coords, space_around) {
        const {
            x,
            y,
        } = coords;
        for (let i = -space_around; i <= space_around; i++) {
            for (let j = -space_around; j <= space_around; j++) {
                if (this.grid[x + space_around][y + space_around]) {
                    return false;
                }
            }
        }
        return true;
    }


    /**
     *
     * @param {Array} grid
     * @return {Array}
     */
    makeListGrid(grid) {
        const result = [];
        grid.forEach((grid_x) => {
            grid_x.forEach((cell) => {
                if (cell !== null) {
                    result.push(cell);
                }
            });
        });
        return result;
    }

    /**
     * @returns {Void}
     */
    broadCastGridToAllUsers() {
        this.user_list.forEach((user) => user.clientSendUpdateGameSession());
    }

    /**
     * @returns {void}
     */
    processUsersMove() {
        this.user_list.forEach((user) => user.processMove());
    }


    /**
     * // Todo, notify every one
     * @returns {void}
     */
    processUsersDeads() {
        const dead_user_list = this.user_list.filter((user) => user.is_alive === false);
        dead_user_list.forEach((dead_user) => {
            this.removeUserById(dead_user.id);
            this.dead_user_list.push(dead_user);
        });
    }


    /**
     * @returns {Array<Object>}
     */
    getItemsList() {
        return this.items_list;
    }

    /**
     * @param {Number} x
     * @param {Number} y
     *
     * @return {Object}
     */
    findItemByCoords(x, y) {
        return this.items_list
            .find((item) => item.getX() === x && item.getY() === y);
    }


    /**
     * @param {Number} x
     * @param {Number} y
     *
     * @return {Integer}
     */
    findItemIndexByCoords(x, y) {
        return this.items_list
            .findIndex((item) => item.getX() === x && item.getY() === y);
    }

    /**
     *
     * @param {Number} x
     * @param {Number} y
     * @returns {void}
     */
    removeItemByCoords(x, y) {
        const index = this.findItemIndexByCoords(x, y);
        this.items_list.splice(index, 1);
    }

    /**
     * @returns {Number}
     */
    getGridWidth() {
        return this.grid_width;
    }


    /**
     * @returns {Number}
     */
    getGridHeight() {
        return this.grid_height;
    }

    /**
     * @param {Number} grid_width
     * @returns {void}
     */
    setGridWidth(grid_width) {
        this.grid_width = grid_width;
    }


    /**
     * @param {Number} grid_height
     * @returns {Number}
     */
    setGridHeight(grid_height) {
        this.grid_height = grid_height;
    }


    /**
     * @returns {String}
     */
    generateNewGameId() {
        return v4();
    }


    /**
     * @return {String}
     */
    getId() {
        return this.game_id;
    }
}

module.exports = {
    GameSession,
};
