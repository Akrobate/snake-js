'use strict';

const {
    logger,
} = require('../logger');
const {
    v4,
} = require('uuid');
const {
    DirectionsReferential,
} = require('./DirectionsReferential');

const {
    GameCell,
} = require('./GameCell');

class UserGameSession {

    /**
     * Snake orientation
     */
    static get ORIENTATION() {
        return {
            HORIZONTAL: 1,
            VERTICAL: 0,
        };
    }

    /**
     * @param {Object} game_session
     * @param {Object} websocket_connection
     * @param {String} user
     */
    constructor(game_session, websocket_connection, user) {
        const {
            name,
            color_name,
        } = user;

        this.score = 0;

        this.game_session = game_session;
        this.websocket_connection = websocket_connection;
        this.name = name;
        this.color_name = color_name;
        this.snake_sections_list = [];
        this.initial_snake_size = 3;

        this.digest_item_list = [];

        this.is_alive = true;

        this.snake_direction = DirectionsReferential.UP;
        this.id = v4();
        this.game_session.addUser(this);
        this.clientSendGameParams();
    }

    /**
     * @param {Number} number_sections
     * @param {Object} spawn_position
     * @param {Number} orientation
     * @returns {void}
     */
    initSnakeSectionsList(
        number_sections,
        spawn_position,
        orientation = UserGameSession.ORIENTATION.HORIZONTAL
    ) {
        const {
            x,
            y,
        } = spawn_position;

        if (orientation === UserGameSession.ORIENTATION.VERTICAL) {
            for (let i = 0; i < number_sections; i++) {
                this.snake_sections_list
                    .push(
                        GameCell
                            .build(x, y + i, GameCell.TYPE_PLAYER_BODY)
                            .setOwner(this.id)
                    );
            }
        } else if (orientation === UserGameSession.ORIENTATION.HORIZONTAL) {
            for (let i = 0; i < number_sections; i++) {
                this.snake_sections_list
                    .push(
                        GameCell
                            .build(x + i, y, GameCell.TYPE_PLAYER_BODY)
                            .setOwner(this.id)
                    );
            }
        }

        this.snake_sections_list[0].setType(GameCell.TYPE_PLAYER_HEAD);
    }


    /**
     * @param {Number} direction
     * @returns {void}
     */
    processMove() {
        this.moveSnake();
        this.outOfScreenMoovedRule();
        if (this.checkHeadIsOnOwnOrOthersBody()) {
            this.processDie();
        }
        this.digestItems();

        if (this.checkHeadIsOnItem()) {
            this.scoreGain();
            this.eatItem();
        }
    }


    /**
     * @returns {Boolean}
     */
    checkHeadIsOnOwnOrOthersBody() {
        const [head] = this.snake_sections_list;
        let head_is_on_body = false;
        const on_head_coords = this.game_session.grid[head.getX()][head.getY()];
        if (on_head_coords !== null && on_head_coords.isTypePlayerBody()) {
            head_is_on_body = true;
        }
        return head_is_on_body;
    }

    /**
     * @returns {Boolean}
     */
    processDie() {
        this.is_alive = false;
    }

    /**
     * @returns {void}
     */
    moveSnake() {

        if (DirectionsReferential.ALL_POSSIBLE_DIRECTIONS.includes(this.snake_direction)) {

            const total_length = this.snake_sections_list.length;
            for (let index = 1; index < total_length; index++) {
                const cell_to_update = this.snake_sections_list[total_length - index];
                const cell_update_from = this.snake_sections_list[total_length - index - 1];
                cell_to_update.setY(cell_update_from.getY());
                cell_to_update.setX(cell_update_from.getX());
            }

            const [head_cell] = this.snake_sections_list;
            switch (this.snake_direction) {
                case DirectionsReferential.UP:
                    head_cell.setY(head_cell.getY() - 1);
                    break;
                case DirectionsReferential.DOWN:
                    head_cell.setY(head_cell.getY() + 1);
                    break;
                case DirectionsReferential.LEFT:
                    head_cell.setX(head_cell.getX() - 1);
                    break;
                case DirectionsReferential.RIGHT:
                    head_cell.setX(head_cell.getX() + 1);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * Needs verification seems not pluggued
     * @returns {void}
     */
    outOfScreenMoovedRule() {
        const [head_cell] = this.snake_sections_list;
        const width_max = this.game_session.getGridWidth() - 1;
        const height_max = this.game_session.getGridHeight() - 1;
        if (head_cell.getX() < 0) {
            head_cell.setX(width_max);
        }
        if (head_cell.getX() > width_max) {
            head_cell.setX(0);
        }
        if (head_cell.getY() < 0) {
            head_cell.setY(height_max);
        }
        if (head_cell.getY() > height_max) {
            head_cell.setY(0);
        }
    }

    /**
     * @returns {void}
     */
    checkHeadIsOnItem() {
        const [head] = this.snake_sections_list;
        const result = this.game_session.findItemByCoords(head.getX(), head.getY());
        return result !== undefined;
    }

    /**
     * @returns {void}
     */
    eatItem() {
        const [head] = this.snake_sections_list;
        this.digest_item_list.push(
            this.game_session.findItemByCoords(head.getX(), head.getY())
        );
        this.game_session.removeItemByCoords(head.getX(), head.getY());
    }

    /**
     * @returns {void}
     */
    digestItems() {
        if (this.checkIfDigestingItemLeavedSnakeBody()) {
            this.snake_sections_list.push(
                GameCell
                    .build(
                        this.digest_item_list[0].getX(),
                        this.digest_item_list[0].getY(),
                        GameCell.TYPE_PLAYER_BODY
                    )
                    .setOwner(this.id)
            );
            this.digest_item_list.splice(0, 1);
        }
    }

    /**
     * @returns {void}
     */
    scoreGain() {
        this.score += 1;
    }

    /**
     * @return {Boolean}
     */
    checkIfDigestingItemLeavedSnakeBody() {
        const indexes = this.snake_sections_list.map((section) => this.digest_item_list
            .findIndex(
                (item) => !(item.getX() === section.getX() && item.getY() === section.getY())
            )
        );
        return indexes.find((item) => item !== 0) === undefined;
    }

    /**
     * @param {Number} direction
     * @returns {void}
     */
    setDirection(direction) {
        if (this.checkDirectionUpdatePossible(direction)) {
            this.snake_direction = direction;
            console.log('Direction update player : ', this.name, ' direction: ', this.snake_direction);
        }
    }

    /**
     * @param {Number} direction
     * @returns {Boolean}
     */
    checkDirectionUpdatePossible(direction) {
        const direction_of_body_relative_to_head = this
            .findDirectionOfFirstSectionOfBodyRelativeToHead(direction);
        return direction_of_body_relative_to_head !== direction;
    }

    /**
     * @returns {Number}
     */
    findDirectionOfFirstSectionOfBodyRelativeToHead() {
        const [
            head,
            first_section_body,
        ] = this.snake_sections_list;

        const delta_x = head.getX() - first_section_body.getX();
        const delta_y = head.getY() - first_section_body.getY();

        if (delta_y === 1) {
            return DirectionsReferential.UP;
        }

        if (delta_y === -1) {
            return DirectionsReferential.DOWN;
        }

        if (delta_x === 1) {
            return DirectionsReferential.LEFT;
        }

        if (delta_x === -1) {
            return DirectionsReferential.RIGHT;
        }
        return null;
    }

    /**
     * @returns {Array}
     */
    getSnakeSectionsList() {
        return this.snake_sections_list;
    }

    /**
     * @returns {void}
     */
    clientSendUpdateGameSession() {
        // logger.log('clientSendUpdateGameSession');
        // logger.log('updating grid for user', this.name);
        this.websocket_connection.send(
            JSON.stringify({
                action: 'update_grid',
                payload: {
                    grid_data: this.game_session.makeListGrid(this.game_session.getGrid()),
                    dead_user_list: this.game_session.dead_user_list.map(this.formatUserItem),
                    user_list: this.game_session.user_list.map(this.formatUserItem),
                    player: this.formatUserItem(this),
                },
            })
        );
    }


    /**
     * @param {Object} user
     * @returns {Object}
     */
    formatUserItem(user) {
        return {
            id: user.id,
            name: user.name,
            color_name: user.color_name,
            score: user.score,
        };
    }


    /**
     * @returns {void}
     */
    clientSendGameParams() {
        logger.log('clientSendGameParams');
        this.websocket_connection.send(
            JSON.stringify({
                action: 'game_params',
                payload: {
                    grid_width: this.game_session.getGridWidth(),
                    grid_height: this.game_session.getGridHeight(),
                },
            })
        );
    }


    /**
     * @return {Object}
     */
    getGameSession() {
        return this.game_session;
    }

    /**
     * @param {GameSession} game_session
     * @return {Object}
     */
    setGameSession(game_session) {
        this.game_session = game_session;
    }
}

module.exports = {
    UserGameSession,
};
