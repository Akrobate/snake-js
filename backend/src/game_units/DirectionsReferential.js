/* eslint-disable require-jsdoc */

'use strict';

class DirectionsReferential {

    static get UP() {
        return 38;
    }

    static get DOWN() {
        return 40;
    }

    static get LEFT() {
        return 37;
    }

    static get RIGHT() {
        return 39;
    }

    static get ALL_POSSIBLE_DIRECTIONS() {
        return [
            DirectionsReferential.UP,
            DirectionsReferential.DOWN,
            DirectionsReferential.LEFT,
            DirectionsReferential.RIGHT,
        ];
    }
}

module.exports = {
    DirectionsReferential,
};
