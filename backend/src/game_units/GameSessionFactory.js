'use strict';

const {
    GameSession,
} = require('./GameSession');

class GameSessionFactory {


    // eslint-disable-next-line require-jsdoc
    static getInstance() {
        if (GameSessionFactory.instance === null) {
            GameSessionFactory.instance = new GameSessionFactory();
        }
        return GameSessionFactory.instance;
    }


    /**
     * @param {Object} game_params
     * @returns {GameSession}
     */
    build(game_params = {}) {
        const game_session = new GameSession();
        game_session.initGameSession(game_params);
        return game_session;
    }

}

GameSessionFactory.instance = null;

module.exports = {
    GameSessionFactory,
};
