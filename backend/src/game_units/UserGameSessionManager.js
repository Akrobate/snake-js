'use strict';

const {
    GameSessionFactory,
} = require('./GameSessionFactory');

const {
    UserGameSession,
} = require('./UserGameSession');


class UserGameSessionManager {

    // eslint-disable-next-line require-jsdoc
    static get NUMBER_OF_USERS_PER_GAME() {
        return 10;
    }

    // eslint-disable-next-line require-jsdoc
    static getInstance() {
        if (UserGameSessionManager.instance === null) {
            UserGameSessionManager.instance = new UserGameSessionManager(
                GameSessionFactory.getInstance()
            );
        }
        return UserGameSessionManager.instance;
    }

    // eslint-disable-next-line require-jsdoc
    constructor(game_session_factory) {
        this.game_session_factory = game_session_factory;
        this.game_session_list = [];
    }

    /**
     * @param {Object} game_params
     * @returns {void}
     */
    addNewGameSession(game_params = {}) {
        const game_session = this.game_session_factory.build(game_params);
        game_session.start();
        this.game_session_list.push(game_session);
        return game_session;
    }


    /**
     * @param {Objct} websocket_connection_handler
     * @param {Objct} data
     * @returns {void}
     */
    addNewUserToGameSession(websocket_connection_handler, data) {
        let game_session = this.findGameSessionWithAvailablePlaceForUser();
        if (game_session === undefined) {
            game_session = this.addNewGameSession();
        }
        return new UserGameSession(
            game_session,
            websocket_connection_handler,
            data
        );
    }


    /**
     * @param {Objct} websocket_connection_handler
     * @param {Objct} data
     * @returns {void}
     */
    createNewGameSession(websocket_connection_handler, data) {
        const {
            game_params,
        } = data;
        const game_session = this.addNewGameSession(game_params);
        return new UserGameSession(
            game_session,
            websocket_connection_handler,
            data
        );
    }


    /**
     * @param {object} user
     * @return {void}
     */
    removeUserFromGameSession(user) {
        const game_sesion = user.getGameSession();
        game_sesion.removeUserById(user.id);
    }

    /**
     * @returns {GameSession | undefined}
     */
    findGameSessionWithAvailablePlaceForUser() {
        return this.game_session_list.find((game_session) => game_session
            .countUsers() < this.getNumberOfUsersPerGame());
    }


    /**
     * // @TODO to test
     * @returns {Array<String>} game session id
     */
    findUnusedGameSessionsIdList() {
        return this.game_session_list
            .filter((game_session) => game_session.countUsers() > 0)
            .map((game_session) => game_session.getId());
    }


    /**
     * @param {String} game_id
     * @return {Void}
     */
    removeGameSession(game_id) {
        const game_session_to_remove = this.game_session_list
            .find((game_session) => game_session.getId() === game_id);
        game_session_to_remove.stop();
        const game_session_to_remove_index = this.game_session_list
            .findIndex((game_session) => game_session.getId() === game_id);
        this.game_session_list
            .splice(game_session_to_remove_index, 1);
    }


    /**
     * // @todo
     * @returns {Void}
     */
    removeUnusedGameSessions() {
        // find
        // iterate over remove
    }


    /**
     * @return {Void}
     */
    stopAllGameSessions() {
        this.game_session_list.forEach((game_session) => {
            game_session.stop();
        });
    }


    /**
     * @return {Void}
     */
    removeGameSessions() {
        this.game_session_list = [];
    }


    /**
     * @return {Void}
     */
    cleanGameSessions() {
        this.stopAllGameSessions();
        this.removeGameSessions();
    }


    /**
     * @returns {Number}
     */
    getNumberOfUsersPerGame() {
        return UserGameSessionManager.NUMBER_OF_USERS_PER_GAME;
    }
}

UserGameSessionManager.instance = null;

module.exports = {
    UserGameSessionManager,
};
