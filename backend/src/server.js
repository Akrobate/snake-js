'use strict';

const {
    configuration,
} = require('./configuration');
const {
    logger,
} = require('./logger');
const {
    server,
} = require('./app');

server.listen(configuration.server.port, () => {
    logger.log('Server runnning');
});
