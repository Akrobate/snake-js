'use strict';

const WebSocket = require('ws');
const {
    createServer,
} = require('http');
const {
    WebSocketController,
} = require('./controllers');

const server = createServer();
const websocket_server = new WebSocket.Server({
    server,
});

WebSocketController
    .getInstance()
    .initWebsocketListeners(websocket_server);

module.exports = {
    server,
    websocket_server,
};
