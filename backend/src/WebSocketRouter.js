'use strict';

class WebSocketRouter {

    // eslint-disable-next-line require-jsdoc
    constructor() {
        this.routes = {};
    }

    /**
     * @param {String} event_name
     * @param {Function} trigger_method
     * @returns {void}
     */
    add(event_name, trigger_method) {
        this.routes[event_name] = trigger_method;
    }

    /**
     * @param {String} event_name
     * @returns {Function}
     */
    matchEvent(event_name) {
        return this.routes[event_name];
    }
}

module.exports = WebSocketRouter;
