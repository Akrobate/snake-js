/* eslint-disable sort-keys */

'use strict';

const grid_width = 300;
const grid_height = 150;

module.exports = {
    server: {
        port: 8090,
    },
    game: {
        grid_width,
        grid_height,
    },
};
