'use strict';

const {
    logger,
} = require('../logger');

const {
    UserGameSessionManager,
} = require('../game_units');
const {
    v4,
} = require('uuid');

class WebSocketController {


    // eslint-disable-next-line require-jsdoc
    static getInstance() {
        if (WebSocketController.instance === null) {
            WebSocketController.instance = new WebSocketController(
                UserGameSessionManager.getInstance()
            );
        }
        return WebSocketController.instance;
    }

    // eslint-disable-next-line no-useless-constructor
    // eslint-disable-next-line require-jsdoc
    constructor(user_game_session_manager) {
        logger.log('WebSocketController inited');
        this.user_game_session_manager = user_game_session_manager;
        this.connected_websocket_list = [];
    }

    /**
     * @param {Object} web_socket
     * @returns {void}
     */
    initWebsocketListeners(web_socket) {
        web_socket.on('connection', (opened_web_socket) => {
            logger.log('New connection detected');
            let user_game_session = null;
            const connection_id = this.addWebSocketConnection(opened_web_socket);

            opened_web_socket.on('message', (message) => {
                const data = this.formatIncomingMessage(message);
                const {
                    action,
                    payload,
                } = data;
                logger.log('received data: ', message);
                switch (action) {
                    case 'start_new_game_session':
                        logger.log('starting');
                        user_game_session = this.user_game_session_manager
                            .addNewUserToGameSession(opened_web_socket, payload);
                        break;
                    case 'create_new_game_session':
                        logger.log('creating game session');
                        user_game_session = this.user_game_session_manager
                            .createNewGameSession(opened_web_socket, payload);
                        break;
                    case 'pause_game_session':
                        logger.log('pause game');
                        user_game_session.getUserSession().pause();
                        break;
                    case 'resume_game_session':
                        logger.log('resume game');
                        user_game_session.getUserSession().start();
                        break;
                    case 'control':
                        logger.log('control changed: ', payload.direction);
                        user_game_session.setDirection(payload.direction);
                        break;
                    case 'screen-test-mode':
                        logger.log('Broadcasting demo to the screen');
                        break;
                    default:
                        logger.log('Unknown action');
                        logger.log(data);
                        break;
                }
            });

            opened_web_socket.on('close', () => {
                logger.log('disconnected client / destroyin user session');
                if (user_game_session) {
                    this.user_game_session_manager
                        .removeUserFromGameSession(user_game_session);
                }
                user_game_session = null;
                this.removeWebSocketConnection(connection_id);
            });

        });
    }

    /**
     * @param {String} message
     * @returns {Object}
     */
    formatIncomingMessage(message) {
        let data = null;
        try {
            data = JSON.parse(message);
            data.payload = data.payload ? data.payload : {};
        } catch (error) {
            data = {
                error,
                message,
            };
        }
        return data;
    }

    /**
     * @param {Object} data
     * @returns {String}
     */
    formatSendMessage(data) {
        return JSON.stringify(data);
    }


    /**
     * @param {Object} connection_handler
     * @return {String}
     */
    addWebSocketConnection(connection_handler) {
        const websocket_uuid = v4();
        this.connected_websocket_list.push({
            id: websocket_uuid,
            connection_handler,
        });
        return websocket_uuid;
    }


    /**
     * @param {String} id
     * @return {Boolean}
     */
    removeWebSocketConnection(id) {
        const index = this.connected_websocket_list
            .findIndex((item) => item.id === id);
        if (index === -1) {
            return false;
        }
        this.connected_websocket_list.splice(index, 1);
        return true;
    }
}

WebSocketController.instance = null;

module.exports = {
    WebSocketController,
};
