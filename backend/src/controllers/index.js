'use strict';

const {
    WebSocketController,
} = require('./WebSocketController');

module.exports = {
    WebSocketController,
};
