'use strict';

const {
    expect,
} = require('chai');

const {
    GameSession,
} = require('../src/game_units');

describe('GameSession', () => {

    it('Should correctly init Empty grid', () => {
        const game_session = new GameSession();
        game_session.initEmptyGrid();
        const grid = game_session.getGrid();

        expect(grid).to.be.an('Array');
        expect(grid.length).to.equal(game_session.grid_width);
        const [
            first_grid_element,
        ] = grid;
        expect(first_grid_element).to.be.an('Array');
        expect(first_grid_element.length).to.equal(game_session.grid_height);
        const [
            first_cell,
        ] = first_grid_element;
        expect(first_cell).to.equal(null);
    });

});
