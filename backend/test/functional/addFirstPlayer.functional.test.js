'use strict';

// https://github.com/davidje13/superwstest

const request = require('superwstest');

const {
    server,
    websocket_server,
} = require('../../src/app');
const {
    expect,
} = require('chai');
const {
    UserGameSessionManager,
} = require('../../src/game_units');


const user_game_session_manager = UserGameSessionManager.getInstance();

describe('My Server', () => {

    beforeEach((done) => {
        server.listen(0, done);
    });

    afterEach((done) => {
        websocket_server.clients.forEach((socket) => {
            socket.close();
        });

        server.close(() => {
            user_game_session_manager.cleanGameSessions();
            done();
        });
    });

    it('start_new_game_session', async () => {
        const user_seed = {
            name: 'Random Name',
            color_name: 'Red',
        };
        await request(server)
            .ws('/')
            .sendJson({
                payload: user_seed,
                action: 'start_new_game_session',
            })
            .expectJson((response) => {
                expect(response).to.have.property('action', 'game_params');
                expect(response).to.have.property('payload').and.to.be.an('Object');
                expect(response.payload).to.have.property('grid_height').and.to.be.a('Number');
                expect(response.payload).to.have.property('grid_width').and.to.be.a('Number');
            })
            .expectJson((response) => {
                expect(response).to.have.property('action', 'update_grid');
                expect(response).to.have.property('payload').and.to.be.an('Object');
                expect(response.payload).to.have.property('grid_data').and.to.be.an('Array');
                expect(response.payload).to.have.property('user_list').and.to.be.an('Array');
                expect(response.payload).to.have.property('dead_user_list').and.to.be.an('Array');
                expect(response.payload).to.have.property('player').and.to.be.an('Object');

                expect(response.payload.player).to.have.property('name', user_seed.name);
                expect(response.payload.player).to.have.property('color_name', user_seed.color_name);
                expect(response.payload.player).to.have.property('id').and.to.be.a('String');
                expect(response.payload.player).to.have.property('score').and.to.be.a('Number');
            })
            .close();
    });


    it('Add two players', async () => {
        const user_seed_1 = {
            name: 'Random Name 1',
            color_name: 'Red',
        };

        const user_seed_2 = {
            name: 'Random Name 2',
            color_name: 'Green',
        };

        let request_handler_user_2 = null;

        await request(server)
            .ws('/')
            .sendJson({
                payload: user_seed_1,
                action: 'start_new_game_session',
            })
            .expectJson((response) => {
                expect(response).to.have.property('action', 'game_params');
                expect(response).to.have.property('payload').and.to.be.an('Object');
                expect(response.payload).to.have.property('grid_height').and.to.be.a('Number');
                expect(response.payload).to.have.property('grid_width').and.to.be.a('Number');
            })
            .expectJson((response) => {
                expect(response).to.have.property('action', 'update_grid');
                expect(response).to.have.property('payload').and.to.be.an('Object');
                expect(response.payload).to.have.property('user_list').and.to.be.an('Array');
                expect(response.payload.user_list.length).to.equal(1);
            })
            .exec(() => {
                request_handler_user_2 = request(server)
                    .ws('/')
                    .sendJson({
                        payload: user_seed_2,
                        action: 'start_new_game_session',
                    })
                    .expectJson((resp) => {
                        expect(resp).to.have.property('action', 'game_params');
                    })
                    .expectJson((response) => {
                        expect(response).to.have.property('action', 'update_grid');
                        expect(response).to.have.property('payload').and.to.be.an('Object');
                        expect(response.payload).to.have.property('user_list').and.to.be.an('Array');
                        expect(response.payload.user_list.length).to.equal(2);
                    });
                return request_handler_user_2;
            })
            .expectJson((response) => {
                expect(response).to.have.property('action', 'update_grid');
                expect(response).to.have.property('payload').and.to.be.an('Object');
                expect(response.payload).to.have.property('user_list').and.to.be.an('Array');
                expect(response.payload.user_list.length).to.equal(2);
            })
            .exec(() => request_handler_user_2.close())
            .expectJson((response) => {
                expect(response).to.have.property('action', 'update_grid');
                expect(response).to.have.property('payload').and.to.be.an('Object');
                expect(response.payload).to.have.property('user_list').and.to.be.an('Array');
                expect(response.payload.user_list.length).to.equal(1);
            })
            .close();
    });
});
