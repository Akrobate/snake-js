'use strict';

// https://github.com/davidje13/superwstest

const request = require('superwstest');
const {
    mock,
} = require('sinon');
const {
    server,
    websocket_server,
} = require('../../src/app');
const {
    expect,
} = require('chai');
const {
    UserGameSessionManager,
} = require('../../src/game_units');


const user_game_session_manager = UserGameSessionManager.getInstance();

describe.only('GameManager', () => {

    const mocks = {};

    beforeEach((done) => {
        server.listen(0, done);
        mocks.user_game_session_manager = mock(user_game_session_manager);
    });


    afterEach((done) => {
        websocket_server.clients.forEach((socket) => {
            socket.close();
        });

        mocks.user_game_session_manager.restore();

        server.close(() => {
            user_game_session_manager.cleanGameSessions();
            done();
        });
    });

    describe('Verify mock modification', () => {

        it('Check users per game count without mock', () => {
            const get_count = user_game_session_manager.getNumberOfUsersPerGame();
            expect(get_count).to.equal(
                UserGameSessionManager.NUMBER_OF_USERS_PER_GAME
            );
        });

        it('Check mock of users per game count', () => {

            const mocked_count = 2;

            mocks.user_game_session_manager
                .expects('getNumberOfUsersPerGame')
                .atLeast(1)
                .returns(mocked_count);

            const get_count = user_game_session_manager.getNumberOfUsersPerGame();
            expect(get_count).to.equal(
                mocked_count
            );
        });
    });


    describe('Check 3 players with 2 user per game generate 2 gamesessions', () => {
        it('Add two players', async () => {

            const mocked_count = 2;

            mocks.user_game_session_manager
                .expects('getNumberOfUsersPerGame')
                .atLeast(1)
                .returns(mocked_count);

            const user_seed_1 = {
                name: 'Random Name 1',
                color_name: 'Red',
            };

            const user_seed_2 = {
                name: 'Random Name 2',
                color_name: 'Green',
            };

            const user_seed_3 = {
                name: 'Random Name 3',
                color_name: 'Yellow',
            };

            let request_handler_user_2 = null;
            let request_handler_user_3 = null;

            await request(server)
                .ws('/')
                .sendJson({
                    payload: user_seed_1,
                    action: 'start_new_game_session',
                })
                .expectJson((response) => {
                    expect(response).to.have.property('action', 'game_params');
                })
                .expectJson((response) => {
                    expect(response).to.have.property('action', 'update_grid');
                    expect(response).to.have.property('payload').and.to.be.an('Object');
                    expect(response.payload).to.have.property('user_list').and.to.be.an('Array');
                    expect(response.payload.user_list.length).to.equal(1);

                    expect(user_game_session_manager.game_session_list.length).to.equal(1);
                })
                .exec(() => {
                    request_handler_user_2 = request(server)
                        .ws('/')
                        .sendJson({
                            payload: user_seed_2,
                            action: 'start_new_game_session',
                        })
                        .expectJson((response) => {
                            expect(response).to.have.property('action', 'game_params');
                        })
                        .expectJson((response) => {
                            expect(response).to.have.property('action', 'update_grid');
                            expect(response).to.have.property('payload').and.to.be.an('Object');
                            expect(response.payload).to.have.property('user_list').and.to.be.an('Array');
                            expect(response.payload.user_list.length).to.equal(2);

                            expect(user_game_session_manager.game_session_list.length).to.equal(1);
                        });
                    return request_handler_user_2;
                })
                .exec(() => {
                    request_handler_user_3 = request(server)
                        .ws('/')
                        .sendJson({
                            payload: user_seed_3,
                            action: 'start_new_game_session',
                        })
                        .expectJson((response) => {
                            expect(response).to.have.property('action', 'game_params');
                        })
                        .expectJson((response) => {
                            expect(response).to.have.property('action', 'update_grid');
                            expect(response).to.have.property('payload').and.to.be.an('Object');
                            expect(response.payload).to.have.property('user_list').and.to.be.an('Array');
                            expect(response.payload.user_list.length).to.equal(1);

                            expect(user_game_session_manager.game_session_list.length).to.equal(2);
                        });
                    return request_handler_user_3;
                })
                .expectJson((response) => {
                    expect(response).to.have.property('action', 'update_grid');
                    expect(response).to.have.property('payload').and.to.be.an('Object');
                    expect(response.payload).to.have.property('user_list').and.to.be.an('Array');
                    expect(response.payload.user_list.length).to.equal(2);
                })
                // .exec(() => request_handler_user_2.close())
                .expectJson((response) => {
                    expect(response).to.have.property('action', 'update_grid');
                    expect(response).to.have.property('payload').and.to.be.an('Object');
                    expect(response.payload).to.have.property('user_list').and.to.be.an('Array');
                    expect(response.payload.user_list.length).to.equal(2);
                })
                .close();
        });
    });


    // Red test
    describe.only('Should remove unused gamesessions', () => {
        it('Add two players', async () => {

            const mocked_count = 2;

            mocks.user_game_session_manager
                .expects('getNumberOfUsersPerGame')
                .atLeast(1)
                .returns(mocked_count);

            const user_seed_1 = {
                name: 'Random Name 1',
                color_name: 'Red',
            };

            const user_seed_2 = {
                name: 'Random Name 2',
                color_name: 'Green',
            };

            const user_seed_3 = {
                name: 'Random Name 3',
                color_name: 'Yellow',
            };

            let request_handler_user_2 = null;
            let request_handler_user_3 = null;

            await request(server)
                .ws('/')
                .sendJson({
                    payload: user_seed_1,
                    action: 'start_new_game_session',
                })
                .expectJson((response) => expect(response).to.have.property('action', 'game_params'))
                .expectJson((response) => expect(response).to.have.property('action', 'update_grid'))
                .exec(() => {
                    request_handler_user_2 = request(server)
                        .ws('/')
                        .sendJson({
                            payload: user_seed_2,
                            action: 'start_new_game_session',
                        })
                        .expectJson((response) => expect(response).to.have.property('action', 'game_params'))
                        .expectJson((response) => expect(response).to.have.property('action', 'update_grid'));
                    return request_handler_user_2;
                })
                .exec(() => {
                    request_handler_user_3 = request(server)
                        .ws('/')
                        .sendJson({
                            payload: user_seed_3,
                            action: 'start_new_game_session',
                        })
                        .expectJson((response) => expect(response).to.have.property('action', 'game_params'))
                        .expectJson((response) => expect(response).to.have.property('action', 'update_grid'));
                    return request_handler_user_3;
                })
                .expectJson((response) => {
                    expect(response).to.have.property('action', 'update_grid');
                    expect(response).to.have.property('payload').and.to.be.an('Object');
                    expect(response.payload).to.have.property('user_list').and.to.be.an('Array');
                    expect(response.payload.user_list.length).to.equal(2);

                    expect(user_game_session_manager.game_session_list.length).to.equal(2);
                })
                .exec(() => request_handler_user_3.close())
                .expectJson((response) => {
                    expect(response).to.have.property('action', 'update_grid');
                    expect(response).to.have.property('payload').and.to.be.an('Object');
                    expect(response.payload).to.have.property('user_list').and.to.be.an('Array');
                    expect(response.payload.user_list.length).to.equal(2);

                    expect(user_game_session_manager.game_session_list.length).to.equal(1);
                })
                .close();
        });
    });
});
